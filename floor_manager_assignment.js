(function ($) {
	Drupal.behaviors.floor_manager_sort_tables = {
		fillSelectUsers: function() {
			$('#select-user').html('');
			$('#user-list').find('li').each(function(){
				var currentli = this;
				var found = false;
				$(".table").find('li').each(function(){
					if($(this).data('users') !== undefined && $(this).data('users') !== null)
					{
						$($(this).data('users').toString().split(',')).each(function() {
                            if(this == $(currentli).data('id')){
								found = true;
							}
                        });
					}
				});
				if($.inArray($('#role-select').val().toString() ,$(this).data('roles').toString().split(",")) >= 0 && !found){
					$('#select-user').append($(this).clone());
				}
			});
		}
	},
	Drupal.behaviors.floor_manager_sort_tables_role_change = {
		attach: function(context) {
			$("#role-select").live("change", function(event) {
				Drupal.behaviors.floor_manager_sort_tables.fillSelectUsers();
			});
		},
	},
	Drupal.behaviors.floor_manager_sort_tables_click = {
		attach: function(context) {
			$(".table").find('li').each(function(){
				var li = $(this);
				li.disableSelection();
				li.click(function(event) {
					if($(li).data('users') !== undefined && $(this).data('users') !== null){
						$(".table").find('li').each(function(){
							$(this).css("border", "1px solid #000");
							$(this).css("color", "#000");
							$(this).css("font-weight", "normal");
						});
						$(li).css("border", "1px solid #090");
						$(li).css("color", "#090");
						$(li).css("font-weight", "bolder");
						$('#table-users').html('<h4>Items for table: ' +
							$(this).text() + '</h4><ul id="user-assignment-list" class="connect-users" style="' +
							'border:1px solid #000; width:100px; height:150px; overflow:auto; margin:0 auto;"></ul>');
						$(li.data('users').toString().split(",")).each(function(){
							var thisid = this;
							$('#user-list').find('li').each(function(){
								if($(this).data('id').toString() == thisid){
									$('#user-assignment-list').append($(this).clone());
								}
							});
						});
						$('#select-user, #user-assignment-list').sortable(
						{
							connectWith: ".connect-users",
							stop: function() {
								var userids = '';
								$('#user-assignment-list').find('li').each(function(){
									userids = userids + $(this).data('id') + ',';
								});
								userids = userids.substring(0, userids.length - 1);
								li.data('users', userids);
								
								var ret = $(li).parent().find('li').map(function() {
									var $this = $(this);
									return {
										id: $this.data('id'),
										val: $this.text(),
										users: $this.data('users')
									};
								}).get();
								
								$(li).parent().parent().find('#edit-field-floor-manager-grid').val(JSON.stringify(ret));
							}
						}
						).disableSelection().css(
						"cursor", "all-scroll");
					}
				});
			});
		},
	},
	Drupal.behaviors.floor_manager_sort_tables_load = {
		attach: function(context) {
			$(document).ready(function(event) {
				Drupal.behaviors.floor_manager_sort_tables.fillSelectUsers();
				$(".table").find('li').each(function(){
					if($(this).text() !== '+') {
						$(this).css("cursor", "pointer");
					}
				});
			});
		}
	}
})(jQuery);
