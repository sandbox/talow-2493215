(function ($) {
	Drupal.behaviors.floor_manager_view = {
		attach: function(context) {
			$(document).ready(function(event) {
				$('.field-type-floor-manager').find('.field-items').find('.floor').find('li').each(function(){
					if($(this).data('users') !== undefined){
						$(this).css("cursor", "pointer");
						$(this).click(function(){
							$('#show-user-list').empty();
							$($(this).data('users').toString().split(',')).each(function(){
								if(this != '')
								{
									$liDataUser = this;
									$('#user-list').find('li').each(function(){
										if($(this).data('id') == $liDataUser)
											$('#show-user-list').append($(this).clone());
									});
								}
							});
						});
					}
				});
			});
		}
	}
})(jQuery);
